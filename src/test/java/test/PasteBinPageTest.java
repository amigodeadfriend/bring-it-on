package test;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import page.PasteBinPage;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Test of pastebin creation with defined parameters")
class PasteBinPageTest {

    private static WebDriver driver;
    private static PasteBinPage pasteBinPage;
    private static final String PASTE_CODE = "git config --global user.name  \"New Sheriff in Town\"\n" +
            "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
            "git push origin master --force";
    private static final String PASTE_NAME_TITLE = "how to gain dominance among developers";

    private static final String SYNTAX_HIGHLIGHTING_OPTION = "Bash";

    private static final String PASTE_EXPIRATION_OPTION = "10 Minutes";

    private static final String EXPECTED_NOTE = "NOTE: Your guest paste has been posted. If you sign up for a free account, you can edit and delete your pastes!";

    private static final String EXPECTED_TYPE_OF_HIGHLIGHTED_TEXT = "Bash";
    @BeforeAll
    public static void setUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pasteBinPage = new PasteBinPage(driver);
    }

    @Test
    @DisplayName("Creating of new bin and check of its parameters")
    void testPageBin() {

        pasteBinPage.openPage();
        pasteBinPage.insertCodeInNewPaste(PASTE_CODE);
        pasteBinPage.chooseSyntaxHighlightingOption(SYNTAX_HIGHLIGHTING_OPTION);
        pasteBinPage.choosePasteExpirationOption(PASTE_EXPIRATION_OPTION);
        pasteBinPage.insertPasteNameTitle(PASTE_NAME_TITLE);
        pasteBinPage.clickCreateNewPasteBtn();

        String actualNote = pasteBinPage.getNoteOfCreatedBin();
        String actualPageTitle = pasteBinPage.getPageTitle();
        String actualPasteCode = pasteBinPage.getPasteCode();
        String actualTypeOfHighlightedText = pasteBinPage.checkHighlightedText();

        assertEquals(EXPECTED_NOTE, actualNote, "New bin was not pasted");
        assertEquals(PASTE_NAME_TITLE, actualPageTitle, "Page title mismatch");
        assertEquals(PASTE_CODE, actualPasteCode, "Page code mismatch");
        assertEquals(EXPECTED_TYPE_OF_HIGHLIGHTED_TEXT, actualTypeOfHighlightedText, "Text was not highlighted");
    }

    @AfterAll
    public static void finishTest() {
        driver.quit();
    }
}
