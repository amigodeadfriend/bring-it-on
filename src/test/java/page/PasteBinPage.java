package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PasteBinPage {
    private final WebDriver driver;
    private static final String PAGE_LINK = "https://pastebin.com/";
    private static final String OPTION_SELECTOR = "//li[text()='%s']";
    @FindBy(xpath = "//textarea[@id='postform-text']")
    private WebElement newCodePasteField;
    @FindBy(xpath = "//span[@id='select2-postform-format-container']")
    private WebElement syntaxHighlightingFieldOptionSelection;
    @FindBy(xpath = "//span[@id='select2-postform-expiration-container']")
    private WebElement pasteExpirationFieldOptionSelection;
    @FindBy(xpath = "//input[@id='postform-name']")
    private WebElement pasteNameTitleField;
    @FindBy(xpath = "//button[text()='Create New Paste']")
    private WebElement createNewPasteBtn;
    @FindBy(xpath = "//div[@class='notice -success -post-view']")
    private WebElement textOfNote;
    @FindBy(xpath = "//div[@class='info-top']")
    private WebElement pageTitle;
    @FindBy(xpath = "//div[@class='source bash']")
    private WebElement pasteCode;
    @FindBy(xpath = "//a[text()='Bash']")
    private WebElement highlightedType;


    public PasteBinPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void openPage() {
        driver.get(PAGE_LINK);
    }

    public void insertCodeInNewPaste(String text) {
        newCodePasteField.sendKeys(text);
    }

    public void chooseSyntaxHighlightingOption(String syntaxHighlightingOption) {
        syntaxHighlightingFieldOptionSelection.click();
        driver.findElement(By.xpath(String.format(OPTION_SELECTOR,syntaxHighlightingOption))).click();
    }

    public void choosePasteExpirationOption(String expirationOption) {
        pasteExpirationFieldOptionSelection.click();
        driver.findElement(By.xpath(String.format(OPTION_SELECTOR,expirationOption))).click();
    }

    public void insertPasteNameTitle(String text) {
        pasteNameTitleField.sendKeys(text);
    }

    public void clickCreateNewPasteBtn() {
        createNewPasteBtn.click();
    }

    public String getNoteOfCreatedBin() {
        return textOfNote.getText();
    }

    public String getPageTitle() {
        return pageTitle.getText();
    }

    public String getPasteCode() {
        return pasteCode.getText();
    }

    public String checkHighlightedText() {
        return highlightedType.getText();
    }
}

